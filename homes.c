#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "homes.h"

// readhomes function
home ** readhome(char * filename)
{
    int arrlen = 50;
    home **arr = (home **)malloc(arrlen * sizeof(home *));
    FILE *f = fopen(filename, "r");
    printf("file opened succesfuly");
    if (!f)
    {
        printf("Can't open file %s\n", filename);
        exit(1);
    }
    
    
    int zip = 0;
    char addr[55];
    int price = 0;
    int area = 0;
    int i = 0;
    
    
    while(fscanf(f, "%d,%49[^,],%d,%d", &zip,addr,&price,&area) != EOF)
    {
        arr[i] = malloc(sizeof(home));
        arr[i]->addr = (char*)malloc((strlen(addr)+1) * sizeof(char));
        strcpy(arr[i]->addr, addr);
        arr[i]->zip = zip;
        arr[i]->price = price;
        arr[i]->area = area;
        i++;
        if (i == arrlen)
        {
            arrlen = (int)(arrlen * 1.25);
            home **newarr = realloc(arr, arrlen * sizeof(home *));
            if (newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    printf("scan complete");
    arr[i] = NULL;
    return arr;
}
    
    int NumHomes( home ** h )
    {
        int i = 0;
        while ( h[i] != NULL )
        {
            i++;
        }
        return i;
    }
    
    int FindZip( int zip, home ** h )
    {
        int i = 0;
        int count = 0;
        while (h[i] != NULL)
        {
            if (h[i]->zip == zip)
            {
                count++;
            }
            i++;
        }
    return count;
}
    
    void HomePrint( int price, home ** h, int range )
    {
        int i = 0;
        while (h[i] != NULL)
        {
            if (( price >= h[i]->price - range) && ( price <= h[i]->price + range))
            {
                printf("Address: %s, Price: %d\n", h[i]->addr, h[i]->price);
            }
            i++;
        }
    }

int main(int argc, char *argv[])
{

    if ( argc < 2)
    {
        printf( "cant find file %s\n", argv[0]);
        exit(1);
    }
    home ** homes;
    homes = readhome(argv[1]);


    int homenum = NumHomes(homes);
    printf("There are %d homes\n", homenum);
    
    // User enters zip code
    int zip = 0;
    printf("Please Enter a Zip code: \n");
    scanf("%d", &zip);
    
    // input is turned into a variable
    int homezip = FindZip( zip, homes);
    printf("There are %d homes in the %d zip code\n", homezip, zip);

    // input price
    int price = 0;
    printf("Please Enter a price:\n");
    scanf("%d", &price);

    // print function
       HomePrint(price, homes, 10000);
       printf("process complete");
}
